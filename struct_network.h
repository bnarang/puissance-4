/**
 * \file struct.h
 * \brief Structures de données pour la communication réseau
 * \author Benjamin NARANG & Adrien LAGRANGE
 * \version 1.0 
 * \date 21/04/13
 */

#ifndef STRUCT_NETWORK_H
#define STRUCT_NETWORK_H


typedef enum { KEYBOARD, CLIENT, SERVER } player_kind;

typedef int keyboard;

typedef struct {
  int server_port;
  server_connection server_conn;
} server;

typedef struct {
  char *client_host;
  int client_port;
  client_connection client_conn;
} client;

typedef struct {
  player_kind player_kind;
  token player_token;
  union {
    client player_client;
    server player_server;
    keyboard player_keyboard;
  } player_data;
} player_content, *player;

#endif
