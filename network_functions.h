/**
 * \file network_functions.h
 * \brief Déclaration des fonctions nécessaires pour le jeu en réseau
 * \author Benjamin NARANG & Adrien LAGRANGE
 * \version 1.0 
 * \date 22/04/13
 */

#ifndef NETWORK_FUNCTIONS_H
#define NETWORK_FUNCTIONS_H


/**
 * \fn void create_player(player player1, player player2, int argc, char* argv[])
 * \brief Fonction qui crée les différents joueurs en dévut de partie en
 * fonction des entrées en ligne de commande
 *
 * \param player1 données représentants le joueur 1
 * \param player2 données représentants le joueur 2
 * \param argc as usual
 * \param argv[] as usual
 * \return void
 */
void create_player(player_content *player, player_content *player2, int argc, char* argv[]);


/**
 * \fn player create_keyboard(token t)
 * \brief Fonction qui alloue dynamiquement un joueur de type keyboard ie local à la machine
 *
 * \param t couleur du joueur à créer CIRCLE ou CROSS
 * \return pointeur sur la structure créée
 */
void create_keyboard(player p, token t);


/**
 * \fn player create_server(token t, int port)
 * \brief Fonction qui alloue dynamiquement un joueur de type server
 *
 * \param t couleur du joueur à créer CIRCLE ou CROSS
 * \param port numero de port pour la connection
 * \return pointeur sur la structure créée
 */
player create_server(token t,int port);


/**
 * \fn player create_client(token t, char* server_name, int port)
 * \brief Fonction qui alloue dynamiquement un joueur de type client
 *
 * \param t couleur du joueur à créer CIRCLE ou CROSS
 * \param server_name nom de l'hote
 * \param port numero de port pour la connection
 * \return pointeur sur la structure créée
 */
player create_client(token t, char* server_name, int port);

#endif
