/**
 * \file struct_game.h
 * \brief structures de données du jeu
 * \author Benjamin NARANG & Adrien LAGRANGE
 * \version 1.0 
 * \date 12/04/13
 */


#ifndef STRUCT_GAME_H
#define STRUCT_GAME_H

#define COLUMN_NB 7 /** \def COLUMN_NB Nombre de colonnes du plateau */
#define LINE_NB 6 /** \def LINE_NB Nombre de lignes du plateau */


/**
 * \enum token
 * \brief Contenu d'une case
 */
typedef enum { NOTHING, CROSS, CIRCLE } token;


/**
 * \struct grid
 * \brief Objet représentant la grille de jeu.
 *
 * Cette structure est composée de deux éléments.
 * Le premier table est un tableau statique à deux
 * dimensions de token représentant la grille avec
 * les jetons qu'elle contient. Le second élément
 * heights est un tableau comptabilisant le nombre
 * de jetons dans chaque colonne.
 */
typedef struct
{
  token table[LINE_NB][COLUMN_NB];
  int heights[COLUMN_NB];
} grid_content, *grid;


/**
 * \struct direction
 * \brief Décrit les 4 directions de déplacement possible
 *
 * DIAG1 = / et DIAG2 = \ */
typedef enum { HORI, VERT, DIAG1, DIAG2 } direction;


#endif
