/**
 * \file main.c
 * \brief Puissance 4
 * \author Benjamin NARANG && Adrien LAGRANGE
 * \version 1.0
 * \date 12/04/13
 *
 * Jouer au puissance 4, seul ou en réseau !
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "server-lib.h"
#include "client-lib.h"
#include "struct_game.h"
#include "struct_network.h"
#include "network_functions.h"
#include "play_game.h"
#include "game_functions.h"


int main(int argc, char *argv[])
{
  player_content *players = malloc(2*sizeof(player_content));
  token winner;

  /* On crée les joueurs en fonction des paramètres entrés par l'utilisateur */
  create_player(&players[0], &players[1], argc, argv);

  /* On exécute la partie */
  winner = play_game(players);

  /* On affiche le gagnant */
  switch(winner)
  {
    case NOTHING:
      printf(" === ÉGALITÉ : PAS DE GAGNANT ! ===\n");
      break;
    case CROSS:
      printf(" === LES CROIX ONT GAGNÉES !!! ===\n");
      break;
    case CIRCLE:
      printf(" === LES CERCLES ONT GAGNÉS !!! ===\n");
      break;
  }

  exit(EXIT_SUCCESS);
}
