# Projet IN104 : Puissance 4 — Jeu en réseau et intelligence artificielle

## Adrien L. — Benjamin N.

Code C pour obtenir un puissance 4 jouable :
* joueur contre joueur ou IA, sur un même ordinateur ;
* joueur contre joueur ou IA, en réseau.

Le niveau de l'intelligence artificielle est paramétrable.

Le code est documenté avec Doxygen.
