/**
 * \file play_game.c
 * \brief Fonction pour l'exécution de la partie
 * \author Benjamin NARANG && Adrien LAGRANGE
 * \version 1.0 
 * \date 12/04/13
 */

#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "struct_game.h"
#include "game_functions.h"
#include "client-lib.h"
#include "server-lib.h"
#include "struct_network.h"


token play_game(player_content players[2])
{
  grid g = create_grid();
  token is_winner=NOTHING;
  int nb = 0; // nb = numéro du joueur en cours
  int column_input;
  bool validation = false;
  token last_player = CIRCLE;

  system("clear");  
  
  while( is_winner == NOTHING && !complete_grid(g) )
  {
    printf("Au tour du joueur ");
    if(players[nb].player_token == CIRCLE)
      printf("'cercle'.\n");
    else
      printf("'croix'.\n");

    if(players[nb].player_kind == KEYBOARD)
    {
      while(validation==false)
      {
        // On demande la colonne du coup jusqu'à ce que se soit valide
        printf("Donnez un numéro de colonne : ");
        scanf("%d", &column_input);
        column_input--; // Parce que pour le joueur le numero de la premier colonne est 1
        validation=validate_input(g->heights[column_input], column_input);

        if (validation == false)
           printf("Coup invalide : réessayez !\n Une colonne entre %d et %d : ", 1, COLUMN_NB);
      }

      // Si on joue en réseau, il faut envoyer le coup qu'on vient de jouer
/*
       if (players[nb+1 % 2].player_kind!=KEYBOARD)
        output(players[nb+1 % 2], column_input);
*/
      // Sinon, on ne fait rien.
      
      // Quelque soit le mode de jeu, on applique le coup en "local"
      //put_token(&g, column_input, players[nb].player_token);
      put_token(g, column_input, players[nb].player_token);
    }
    
    system("clear"); // Efface la console. Marche sous Unix, pour Windows il faut appeler "cls"
    print_grid(g);
    is_winner = winner(&g,column_input);

    validation=false; // Pour repartir dans la boucle

  }

}
