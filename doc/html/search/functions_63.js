var searchData=
[
  ['create_5fclient',['create_client',['../network__functions_8c.html#a07dac4c9c5d4ebb4cd73404eca6b3e16',1,'create_client(token t, char *server_name, int port):&#160;network_functions.c'],['../network__functions_8h.html#a07dac4c9c5d4ebb4cd73404eca6b3e16',1,'create_client(token t, char *server_name, int port):&#160;network_functions.c']]],
  ['create_5fkeyboard',['create_keyboard',['../network__functions_8c.html#a1f9672248523432072f933f44ad5ea7c',1,'create_keyboard(token t):&#160;network_functions.c'],['../network__functions_8h.html#a1f9672248523432072f933f44ad5ea7c',1,'create_keyboard(token t):&#160;network_functions.c']]],
  ['create_5fplayer',['create_player',['../network__functions_8c.html#a9eca861fe3c82847e1a3680e1c49f7a4',1,'create_player(player player1, player player2, int argc, char *argv[]):&#160;network_functions.c'],['../network__functions_8h.html#a9eca861fe3c82847e1a3680e1c49f7a4',1,'create_player(player player1, player player2, int argc, char *argv[]):&#160;network_functions.c']]],
  ['create_5fserver',['create_server',['../network__functions_8c.html#a7f5cbb678a6e183d3c507aa3aeea7b37',1,'create_server(token t, int port):&#160;network_functions.c'],['../network__functions_8h.html#a7f5cbb678a6e183d3c507aa3aeea7b37',1,'create_server(token t, int port):&#160;network_functions.c']]]
];
