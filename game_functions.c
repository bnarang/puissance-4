/**
 * \file functions.c
 * \brief Fonctions sur la structure de données
 * \author Benjamin NARANG
 * \version 1.0 
 * \date 12/04/13
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "struct_game.h"
#include "game_functions.h"


grid create_grid(void)
{
  grid g=malloc(sizeof(grid_content));
  //assert(g);

  int i,j;
  for (i=0;i<=COLUMN_NB-1;i++)
  {
    g->heights[i] = 0;
    for (j=0;j<=LINE_NB-1;j++)
      { g->table[j][i] = NOTHING; }
  }

  return g;
}


void print_grid(grid g)
{
  int i,j;
  for (i=LINE_NB-1;i>=0;i--)
  {
    if (i==LINE_NB-1)
      { printf("+ - - - - - - - +\n"); }
    for (j=0;j<=COLUMN_NB-1;j++)
    {
      if (j==0)
        { printf("| "); }
      /* On fait un switch sur la valeur du token
       * en (i,j) de la grille */
      switch(g->table[i][j])
      {
        case NOTHING:
          printf("  "); break;
        case CROSS :
          printf("x "); break;
        case CIRCLE:
          printf("o "); break;
      }
      if (j==COLUMN_NB-1)
        { printf("|"); }
    }
    printf("\n");
    if (i==0)
    {
      printf("+ - - - - - - - +\n");
      printf("  1 2 3 4 5 6 7  \n");
    }
  }
  printf("\n");
}


bool validate_input(int i, int j)
{
  if (j<0 || j>=COLUMN_NB)
    { return false; }
  else
  {
    if (i<0 || i>=LINE_NB)
      { return false; }
    else
      { return true; }
  }
}


int put_token(grid g, int j, token t)
{
  int i = g->heights[j];

  g->table[i][j] = t;
  g->heights[j]++;

  return 0;
}


token winner(grid g, int j)
{
  int i=g->heights[j]-1;
  token t=g->table[i][j]; /* t est le token qui vient d'être posé */
  token winner;
  if (t==CROSS)
    winner=CROSS;
  else
    winner=CIRCLE;

  /* 1/ On compte le nombre de token de la même
   * forme sur la même ligne.
   * nb_token = nombre de token qui touchent le
   * dernier token posé et qui sont de la même
   * formen que lui. */
  int nb_token;
  /* On compte le nombre de token de la même forme que t, qui le
   * touchent dans la même ligne, dans un éloignement de 4 max. */
  nb_token = count_token(g, HORI, t, j);
  if (nb_token>=4)
    return winner;

  /* 2/ On fait la même chose verticalement */
  nb_token=count_token(g, VERT, t, j);
  if (nb_token>=4)
    return winner;

  /* 3/ Diagonales / */
  nb_token=count_token(g, DIAG1, t, j);
  if (nb_token>=4)
    return winner;

  /* 4/ Diagonales \ */
  nb_token=count_token(g, DIAG2, t, j);
  if (nb_token>=4)
    return winner;

  /* Si on arrive à la fin, c'est que le joueur
   * n'a pas gagné : il n'y a pas 4 jetons alignés dans
   * aucune des 4 directions. */
  return NOTHING;
}


int count_token(grid g, direction dir, token t, int j)
{
  int nb_token=0,move_line=0,move_column=0,deplacement=0;
  int i=g->heights[j]-1;

  while ( deplacement<=3
          && validate_input(i+move_line, j+move_column)
          && g->table[i+move_line][j+move_column]==t )
  {
    deplacement++;
    nb_token++;
    if (dir==HORI) /* horizontal : on commence par aller à gauche. */
      { move_column--; }
    if (dir==VERT) /* vertical : en bas. */
      { move_line--; }
    if (dir==DIAG1) /* diagonale / : en bas à gauche. */
      { move_line--; move_column--; }
    if (dir==DIAG2)/* diagonale \ : en bas à droite. */
      { move_line--; move_column++; }
  }
  
  deplacement=0;
  move_line=0;
  move_column=0;
  while ( deplacement<=3
          && validate_input(i+move_line, j+move_column)
          && g->table[i+move_line][j+move_column]==t )
  {
    deplacement++;
    nb_token++;
    if (dir==HORI) /* horizontal : on va maintenant à droite. */
      { move_column++; }
    if (dir==VERT) /* vertical : en haut. */
      { move_line++; }
    if (dir==DIAG1) /* diagonale / : en haut à droite. */
      { move_line++; move_column++; }
    if (dir==DIAG2) /* diagonale \ : en haut à gauche. */
      { move_line++; move_column--; }
  }

  nb_token-=1; /* On retranche 1 car dans chaque cas, on compte une fois le
                * token t pour chaque "semi-direction" */

  return nb_token;
}


bool complete_grid(grid g)
{
  int i;

  for (i=0;i<COLUMN_NB;i++)
  {
    if (g->heights[i]<LINE_NB)
      return false; 
  }

  return true;
}
