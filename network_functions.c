/**
 * \file network_functions.c
 * \brief Ensemble des fonctions pour le jeu en réseau
 * \author Benjamin NARANG && Adrien LAGRANGE
 * \version 1.0 
 * \date 22/04/13
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "server-lib.h"
#include "client-lib.h"
#include "struct_game.h"
#include "struct_network.h"
#include "network_functions.h"


void create_player(player_content *player1, player_content *player2, int argc, char* argv[])
{
  if ( strcmp(argv[1],"-keyboard")==0 && strcmp(argv[2],"-keyboard")==0 )
  {
    create_keyboard(player1, CROSS);
    create_keyboard(player2, CIRCLE);
  }
/*
  if (argc==4 && strcmp(argv[1],"-keyboard") && strcmp(argv[2],"-server"))
  {
    player1 = create_keyboard(CROSS);
    player2 = create_server(CIRCLE, atoi(argv[3]) );
  }

  if (argc==5 && strcmp(argv[1],"-client") && strcmp(argv[4],"-keyboard"))
  {
    player1 = create_client(CROSS, argv[2], atoi(argv[3]) );
    player2 = create_keyboard(CIRCLE);
  }*/
}


void create_keyboard(player p, token t)
{
  p->player_kind = KEYBOARD;
  p->player_token = t;
  p->player_data.player_keyboard = 1;
}


player create_server(token t,int port)
{
  player player_created;

  player_created = malloc(sizeof(player_content));
  // assert(player_created);

  player_created->player_kind = SERVER;
  player_created->player_token = t;

  player_created->player_data.player_server.server_port = port;
  player_created->player_data.player_server.server_conn = server_open_connection(port);

  return player_created;
}


player create_client(token t, char* server_name, int port)
{
  player player_created;

  player_created = malloc(sizeof(player_content));
  // assert(player_created);

  player_created->player_kind = CLIENT;
  player_created->player_token = t;

  player_created->player_data.player_client.client_host = server_name;
  player_created->player_data.player_client.client_port = port;
  player_created->player_data.player_client.client_conn = client_open_connection(server_name, port);

  return player_created;
}
