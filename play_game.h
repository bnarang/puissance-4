/**
 * \file play_game.h
 * \brief 
 * \author Benjamin NARANG && Adrien LAGRANGE
 * \version 1.0 
 * \date 12/04/13
 */

#ifndef PLAY_GAME_H
#define PLAY_GAME_H

/**
 * \fn is_winner play_game(player player1, player player2)
 * \brief Déroulement d'une partie entre deux joueurs quelconques
 *
 * \param players[2] est un tableau composé des deux joueurs (player)
 * \return is_winner le gagnant (WIN_CROSS ou WIN_CIRCLE) ou NOBODY si pas
 * de gagnant
 *
 * Cette fonction s'occupe du déroulement d'une partie, selon le type des
 * joueurs. Elle appelle l'ensemble des fonctions game et network.
 */
token play_game(player_content players[2]);

#endif
