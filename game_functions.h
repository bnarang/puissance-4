/**
 * \file functions.h
 * \brief prototypes des functions de manipulation
 * de structures
 * \author Benjamin NARANG
 * \version 1.0 
 * \date 12/04/13
 */

#ifndef FUNCTIONS_H 
#define FUNCTIONS_H


/**
 * \fn grid create_grid(void)
 * \brief Allocation et initialisation d'un grid
 *
 * \param void
 * \return La grid crée
 */
grid create_grid(void);


/**
 * \fn void print_grid(grid g)
 * \brief Affichage du plateau
 *
 * \param g la grille
 * \return void
 */
void print_grid(grid g);


/**
 * \fn bool validate_input(int i, int j)
 * \brief Vérifie si le coup (i,j) est valide (dans la grille)
 *
 * \param i numéro de la ligne dans laquelle le joueur veut jouer
 * \param j numéro de la colonne dans laquelle le joueur veut jouer
 * \return bool false si coup non valide et true sinon
 *
 * Dans la pratique on vérifie si le joueur a entré un numéro qui
 * correspond à une colonne existante
 * et si la colonne n'est pas déjà pleine.
 */
bool validate_input(int i, int j);


/**
 * \fn int put_token(grid g, int j)
 * \brief Place un token dans la grille
 *
 * \param g la grille
 * \param j numéro de la colonne à laquelle on ajoute un token
 * \return 0 si le token a été ajouté 
 * 
 * Attention : on suppose que le coup est valide, la fonction n'effectue
 * pas elle-même la vérification !
 */
int put_token(grid g, int column, token t);


/**
 * \fn is_winner winner(grid g, int j) 
 * \brief Teste si le dernier coup est gagnant.
 *
 * \param g la grille
 * \param j le colonne où le dernier coup a été joué
 * \return is_winner le gagnant : WIN_CROSS, WIN_CIRCLE ou WIN_NOBODY
 *
 * is_winner regarde si le dernier joueur a gagné, en regardant si le token
 * qui vient d'être posé a aligné avec 4 autres dans une des directions
 * (cette fonction est remplie par la fonction count_token).
 * is_winner se charge de calculer l'ordonnée et la forme du token
 * correspondant.
 */
token winner(grid g, int j);


/**
 * \fn int count_token(grid*g, direction dir, token t, int i, int j)
 * \brief Compte si il y a un alignement de 4 token identiques.
 *
 * \param g la grille
 * \param dir la direction (HORI, VERT, DIAG1, DIAG2)
 * \param t le dernier token à avoir été posé
 * \param la colonne où il a été posé
 * \return nb_token le nobre de token aligné dans la direction demandée 
 *
 * Cette fonction retourne le nombre de token identiques dans une des 4
 * directions du jeu, et ce dans un "rayon" de 3 token à gauche et à
 * droite (car il suffit de vérifier cela dans ce rayon pour certifier la
 * victoire ou la non-victoire).
 * Pour cela, elle compte le nombre de token identiques dans chaque
 * "demi-direction" : à gauche puis à droite, en haut puis en bas, etc...
 */
int count_token(grid g, direction dir, token t, int j);


/**
 * \fn bool complete_grid(grid g)
 * \brief Vérifie si la grille est pleine
 *
 * \param *g la grille
 * \return true si la grille est pleine et false sinon
 */
bool complete_grid(grid g);

#endif
