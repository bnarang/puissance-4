#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "client-lib.h"

#define error(args...) do { fprintf(stderr, args); exit(1); } while(0);

struct _client_connection {
  int socket_desc;
};

client_connection client_open_connection(const char* host, int port){
  struct hostent *he;
  struct sockaddr_in server;
  int socket_desc;
  client_connection sc;

  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  if(socket_desc == 0)
    error("Error: failed to create socket\n");

  he = gethostbyname(host);
  if(he == NULL)
    error("Error: cannot resolve host: '%s'\n", host);

  memcpy(&server.sin_addr, he->h_addr_list[0], he->h_length);
  server.sin_family = AF_INET;
  server.sin_port = htons(port);

  if(connect(socket_desc, (struct sockaddr*) &server, sizeof(server)))
    error("Error: connection to server '%s' failed\n", host);

  sc = malloc(sizeof(struct _client_connection));
  sc->socket_desc = socket_desc;

  return sc;
}

void client_send_message(client_connection cc, const char *msg){
  send(cc->socket_desc, msg, strlen(msg), 0);
  send(cc->socket_desc, "\n", 1, 0);
}

void client_receive_message(client_connection cc, char *buf, int len){
  char c = 'x';
  int i = 0, n = 1;
  while(n != 0 && c != '\n' && c != '\0'){
    if(i >= len) error("Error: client receive message: buffer overflow\n");
    n = recv(cc->socket_desc, &c, 1, 0);
    if(n == -1) error("Error: client receive message\n");
    buf[i] = c;
    i ++;
  }
  buf[i-1] = '\0';
}

void client_close_connection(client_connection cc){
  close(cc->socket_desc);
  free(cc);
}
